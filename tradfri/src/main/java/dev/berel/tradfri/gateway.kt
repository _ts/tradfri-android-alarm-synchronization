package dev.berel.tradfri

import kotlinx.serialization.json.*
import kotlinx.serialization.parse
import org.eclipse.californium.core.CoapClient
import org.eclipse.californium.core.coap.MediaTypeRegistry
import org.eclipse.californium.core.network.CoapEndpoint
import org.eclipse.californium.scandium.DTLSConnector
import org.eclipse.californium.scandium.config.DtlsConnectorConfig
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore

data class TradfriGatewayConfiguration(
    val ip: String, val port: String = "5684",
    val secUser: String, val secCode: String
) {
    val base = "coaps://${ip}:${port}"
}

private val json = Json(JsonConfiguration.Stable.copy(ignoreUnknownKeys = true))

class TradfriGateway(val config: TradfriGatewayConfiguration) {

    private val endpoint by lazy {
        val dtls = DTLSConnector(
            DtlsConnectorConfig.Builder()
                .setPskStore(StaticPskStore(config.secUser, config.secCode.toByteArray()))
                .build()
        )

        CoapEndpoint.Builder().setConnector(dtls).build()
    }

    fun ping(): Boolean = CoapClient(config.base).setEndpoint(endpoint).ping(100)

    fun devices(): List<Device> =
        CoapClient("${config.base}/15001").setEndpoint(endpoint).get().takeIf { it.isSuccess }
            ?.let {
                json.parseJson(it.responseText).jsonArray.mapNotNull {
                    CoapClient("${config.base}/15001/${it.int}").setEndpoint(endpoint).get()
                        .takeIf { it.isSuccess }
                        ?.let {
                            json.parse(Device.serializer(), it.responseText)
                        }
                }
            } ?: emptyList()

    fun device(id: Int): Device? =
        CoapClient("${config.base}/15001/${id}").setEndpoint(endpoint).get()
            .takeIf { it.isSuccess }?.let {
                json.parse(Device.serializer(), it.responseText)
            }

    fun lamps(): List<Device> =
        devices().filter { it.bulb != null && it.bulb.size == 1 }

    fun update(device: Device): Boolean {
        val payload = json.stringify(Device.serializer(), device)

        return CoapClient("${config.base}/15001/${device.id}").setEndpoint(endpoint)
            .put(payload, MediaTypeRegistry.TEXT_PLAIN).isSuccess
    }

    fun task(id: Int): Task? =
        CoapClient("${config.base}/15010/${id}").setEndpoint(endpoint).get()
            .takeIf { it.isSuccess }?.let {
                json.parse(Task.serializer(), it.responseText)
            }

    fun tasks(): List<Task> {
        return CoapClient("${config.base}/15010/").setEndpoint(endpoint).get()
            .takeIf { it.isSuccess }?.let {
                json.parseJson(it.responseText).jsonArray.mapNotNull {
                    task(it.int)
                }
            } ?: emptyList()
    }

    fun update(task: Task): Boolean {
        val payload = json.stringify(Task.serializer(), task)

        return CoapClient("${config.base}/15010/${task.id}").setEndpoint(endpoint)
            .put(payload, MediaTypeRegistry.TEXT_PLAIN).isSuccess
    }

    fun add(task: Task): Boolean {
        val payload = json.stringify(Task.serializer(), task)

        return CoapClient("${config.base}/15010/").setEndpoint(endpoint)
            .put(payload, MediaTypeRegistry.TEXT_PLAIN).isSuccess
    }

    fun deleteTask(id: Int): Boolean =
        CoapClient("${config.base}/15010/${id}").setEndpoint(endpoint)
            .delete().isSuccess

    companion object {
        fun authenticate(config: TradfriGatewayConfiguration, gatewayKey: String): String {
            val endpoint = DTLSConnector(
                DtlsConnectorConfig.Builder()
                    .setPskStore(StaticPskStore("Client_identity", gatewayKey.toByteArray()))
                    .build()
            ).let {
                CoapEndpoint.Builder().setConnector(it).build()
            }

            val auth = CoapClient("${config.base}/15011/9063").setEndpoint(endpoint)
                .post("{\"9090\":\"${config.secUser}\"}", MediaTypeRegistry.TEXT_PLAIN)

            return json.parseJson(auth.responseText).jsonObject["9091"]?.content!!
        }
    }
}
