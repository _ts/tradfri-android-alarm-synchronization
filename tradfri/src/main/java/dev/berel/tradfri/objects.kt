package dev.berel.tradfri

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Action(
    @SerialName("5850")
    val state: Int?,
    @SerialName("15013")
    val lightSettings: List<LightSetting>? = null
)

@Serializable
data class LightSetting(
    @SerialName("9003")
    val bulb: Int,
    @SerialName("5851")
    val intensity: Int = 0,
    @SerialName("5706")
    val color: String?,
    @SerialName("5712")
    val duration: Int = 0
)

@Serializable
data class TriggerInterval(
    @SerialName("9046")
    val startHour: Int?,
    @SerialName("9047")
    val startMinute: Int?,
    @SerialName("9048")
    val endHour: Int?,
    @SerialName("9049")
    val endMinute: Int?
)

@Serializable
data class Task(
    @SerialName("9001")
    val name: String,
    @SerialName("9003")
    val id: Int?,
    @SerialName("9041")
    val repeat: Int, // binary: Sunday = 64 ... Monday = 1, Monday & Sunday = 65 and don't repeat = 0
    @SerialName("9042")
    val startAction: Action?,
    @SerialName("9043")
    val endAction: Action? = null,
    @SerialName("5850")
    val state: Int?,
    @SerialName("9040")
    val type: Int?,
    @SerialName("9044")
    val triggerIntervals: List<TriggerInterval>?
)

@Serializable
data class Bulb(
    @SerialName("5850")
    val state: Int,
    @SerialName("5851")
    val intensity: Int,
    @SerialName("5711")
    val colorTemp: Int? = null // 250 <= x < 455
)

@Serializable
data class Device(
    @SerialName("9001")
    val name: String,
    @SerialName("9003")
    val id: Int?,
    @SerialName("5750")
    val type: Int,
    @SerialName("3311")
    val bulb: List<Bulb>? = null
)