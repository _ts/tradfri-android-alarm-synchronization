package dev.berel.tradfrialarmsync

import android.app.AlarmManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.fragment.app.DialogFragment
import dev.berel.tradfri.TradfriGateway
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.list
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import java.util.*

private val json = Json(JsonConfiguration.Stable.copy(ignoreUnknownKeys = true))

object PreferenceKeys {
    val TradfriGatewaySecureUser = "TradfriGatewaySecureUser"
    val TradfriGatewaySecureKey = "TradfriGatewaySecureKey"
    val TradfriGatewayIp = "TradfriGatewayIp"

    val TaskId = "TaskId"
    val BulbSettings = "BulbSettings"
}

val Context.preferences: SharedPreferences
    get() = getSharedPreferences("dev.berel.tradfrialarmsync", Context.MODE_PRIVATE)

fun SharedPreferences.loadLampSettings(): List<BulbSettings> = try {
    json.parse(
        BulbSettings.serializer().list,
        getString(PreferenceKeys.BulbSettings, "")!!
    )
} catch (e: Throwable) {
    listOf()
}

@Serializable
data class BulbSettings(val id: Int, val name: String, val intensity: Int, val toggle: Boolean)

class MainActivity : AppCompatActivity() {

    class BulbListAdapter : BaseAdapter() {
        var enabled = true
            set(v) {
                field = v
                notifyDataSetChanged()
            }

        private var _lamps: MutableList<BulbSettings> = mutableListOf()
        var lamps: List<BulbSettings>
            set(v) {
                _lamps = v.toMutableList()
                notifyDataSetChanged()
            }
            get() = _lamps.toList()

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = convertView ?: LayoutInflater.from(parent.context)
                .inflate(R.layout.bulb_listelement, parent, false) as View

            val name = view.requireViewById<TextView>(R.id.bulb_name)
            val toggle = view.requireViewById<Switch>(R.id.bulb_toggle)
            val intensity = view.requireViewById<SeekBar>(R.id.bulb_intensity)

            _lamps[position].let {
                name.apply {
                    text = it.name
                }
                toggle.apply {
                    isEnabled = this@BulbListAdapter.enabled
                    isChecked = it.toggle
                    setOnCheckedChangeListener { buttonView, isChecked ->
                        _lamps[position] = it.copy(toggle = isChecked)
                    }
                }
                intensity.apply {
                    isEnabled = this@BulbListAdapter.enabled
                    progress = it.intensity
                    setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                        override fun onProgressChanged(
                            seekBar: SeekBar?,
                            progress: Int,
                            fromUser: Boolean
                        ) {
                            _lamps[position] = it.copy(intensity = progress)
                        }

                        override fun onStartTrackingTouch(seekBar: SeekBar?) {}

                        override fun onStopTrackingTouch(seekBar: SeekBar?) {}
                    })
                }
            }

            return view
        }

        override fun getItem(position: Int): Any = _lamps[position]

        override fun getItemId(position: Int): Long = _lamps[position].id.toLong()

        override fun getCount(): Int = _lamps.size
    }

    lateinit var settingsDialog: DialogFragment

    val adapter: BulbListAdapter = BulbListAdapter()
    lateinit var bulbListView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bulbListView = findViewById(R.id.bulb_list)
        bulbListView.adapter = adapter

        settingsDialog =
            ConnectSettingsDialog { ip, key ->
                if (key.isNotBlank() && ip.isNotBlank()) {
                    // new auth
                    val user = UUID.randomUUID().toString()

                    preferences.createGatewayConfig(
                        ip = ip,
                        user = user,
                        key = ""
                    )?.let { config ->
                        Log.d("gateway", "Authenticate")
                        TradfriGateway.safeIf({ authenticate(config, key) }, { this.isNotBlank() })
                            .onLog("gateway", "authenticated", "couldn't authenticate")
                            .on { token ->
                                preferences.edit(commit = true) {
                                    putString(
                                        PreferenceKeys.TradfriGatewaySecureKey,
                                        token
                                    )
                                    putString(
                                        PreferenceKeys.TradfriGatewaySecureUser,
                                        user
                                    )
                                    putString(
                                        PreferenceKeys.TradfriGatewayIp,
                                        ip
                                    )
                                }
                            }
                    }
                } else if (ip.isNotBlank()) {
                    preferences.edit(commit = true) {
                        putString(
                            PreferenceKeys.TradfriGatewayIp,
                            ip
                        )
                    }
                }

                populateAdapter()
            }

        // connection dialog
        toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.settings_show) {
                savePreferences()

                settingsDialog.show(supportFragmentManager, null)
            }
            true
        }

        // populate adapter with the saved settings
        if (adapter.lamps.isEmpty()) {
            adapter.lamps = preferences.loadLampSettings()
        }
    }

    override fun onResume() {
        super.onResume()

        populateAdapter()
    }

    fun populateAdapter() {
        // check if gateway is reachable
        preferences.createGatewayConfig()?.let { config ->
            TradfriGateway(config).let { gateway ->
                gateway.safeIf { ping() }.on({
                    adapter.enabled = true

                    adapter.lamps = preferences.loadLampSettings().let {
                        mergeWithLampsFromGateway(it).sortedBy { it.id }
                    }
                }, {
                    adapter.enabled = false
                })
            }
        }
    }

    /**
     * Update settings list with the settings from the gateway.
     */
    fun mergeWithLampsFromGateway(lampSettings: List<BulbSettings>): List<BulbSettings> =
        preferences.createGatewayConfig()?.let { config ->
            TradfriGateway(config).let { gateway ->
                gateway.safe { lamps() }.on {
                    it.map { l ->
                        lampSettings.find { it.id == l.id }
                            ?: BulbSettings(
                                id = l.id!!,
                                name = l.name,
                                intensity = 0,
                                toggle = false
                            )
                    }
                }
            }
        } ?: lampSettings

    override fun onPause() {
        super.onPause()

        savePreferences()
        syncGateway()
    }

    fun savePreferences() {
        preferences.edit(commit = true) {
            putString(
                PreferenceKeys.BulbSettings,
                json.stringify(BulbSettings.serializer().list, adapter.lamps)
            )
        }
    }
}

/**
 * Listens to `ACTION_NEXT_ALARM_CLOCK_CHANGED` events. Enables/updates the task accordingly.
 */
class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == AlarmManager.ACTION_NEXT_ALARM_CLOCK_CHANGED) {
            context.syncGateway()
        }
    }
}