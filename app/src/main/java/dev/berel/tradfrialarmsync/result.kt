package dev.berel.tradfrialarmsync

import android.util.Log

sealed class Result<R>
data class Success<R>(val data: R) : Result<R>()
data class Failure<R>(val error: String) : Result<R>()

/**
 * Calls [success] or [failure] and returns its result.
 */
inline fun <T, R> Result<T>.on(success: (T) -> R, failure: (error: String) -> R): R =
    when (this) {
        is Success -> success(this.data)
        is Failure -> failure(this.error)
    }

/**
 * Calls [success] and returns its result or null if the [Result] is of type [Failure].
 */
inline fun <T, R> Result<T>.on(success: (T) -> R): R? =
    when (this) {
        is Success -> success(this.data)
        else -> null
    }

fun <T> Result<T>.onLog(tag: String, success: String, failure: String): Result<T> {
    this.on({ Log.d(tag, success) }, { Log.d(tag, failure) })
    return this
}

// wraps a try ... catch block and enables a function style

/**
 * Calls [block] with `this` value as its receiver and returns its result.
 */
inline fun <T, R> T.safe(block: T.() -> R): Result<R> =
    try {
        Success(block(this))
    } catch (e: Throwable) {
        Failure(e.message ?: "")
    }

inline fun <T> T.safeIf(block: T.() -> Boolean): Result<Boolean> =
    this.safeIf(block, { this })

/**
 * Calls [block] with `this` value as its receiver, checks the result within [condition] and returns its result.
 * The result of the [condition] equals to a [Success] or [Failure].
 */
inline fun <T, R> T.safeIf(block: T.() -> R, condition: R.() -> Boolean): Result<R> =
    try {
        block(this).let {
            if (it.condition()) Success(it) else Failure("condition not matched")
        }
    } catch (e: Throwable) {
        Failure(e.message ?: "")
    }