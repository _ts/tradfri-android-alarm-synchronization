package dev.berel.tradfrialarmsync

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class ConnectSettingsDialog(val callback: (ip: String, key: String) -> Unit) : DialogFragment() {
    lateinit var tokenSwitch: Switch
    lateinit var keyEdit: TextView
    lateinit var ipEdit: TextView

    override fun onResume() {
        super.onResume()

        requireActivity().let { activity ->
            requireDialog().let {
                ipEdit.text = activity.preferences.getString(PreferenceKeys.TradfriGatewayIp, "")

                if (activity.preferences.getString(PreferenceKeys.TradfriGatewaySecureKey, "")
                        .isNullOrBlank()
                ) {
                    tokenSwitch.isChecked = true
                    tokenSwitch.isEnabled = false
                    keyEdit.isEnabled = true
                } else {
                    tokenSwitch.isChecked = false
                    tokenSwitch.isEnabled = true
                    keyEdit.isEnabled = false
                }
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)

        return requireActivity().let {
            AlertDialog.Builder(it).apply {
                setCancelable(true)
                setView(layoutInflater.inflate(R.layout.settings_connect, null).apply {
                    tokenSwitch = findViewById<Switch>(R.id.settings_new_token)
                    keyEdit = findViewById<TextView>(R.id.settings_gateway_code)
                    ipEdit = findViewById<TextView>(R.id.settings_gateway_ip)

                    findViewById<Switch>(R.id.settings_new_token)
                        .setOnCheckedChangeListener { buttonView, isChecked ->
                            findViewById<View>(R.id.settings_gateway_code).isEnabled = isChecked
                        }
                })
                setNegativeButton(R.string.settings_cancel) { dialog, which ->
                    callback("", "")
                }

                setPositiveButton(R.string.settings_ok) { dialog, which ->
                    callback(ipEdit.text.toString(), if(tokenSwitch.isChecked) keyEdit.text.toString() else "")
                }
            }.create()
        }
    }
}