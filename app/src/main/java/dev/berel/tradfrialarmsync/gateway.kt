package dev.berel.tradfrialarmsync

import android.app.AlarmManager
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import dev.berel.tradfri.*
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.ChronoField
import java.time.temporal.ChronoUnit

private const val TAG = "gateway"

fun SharedPreferences.createGatewayConfig(
    user: String? = null,
    key: String? = null,
    ip: String? = null
): TradfriGatewayConfiguration? = run {
    val user = user ?: getString(PreferenceKeys.TradfriGatewaySecureUser, null)
    val key = key ?: getString(PreferenceKeys.TradfriGatewaySecureKey, null)
    val ip = ip ?: getString(PreferenceKeys.TradfriGatewayIp, null)

    if (user != null && key != null && ip != null) {
        TradfriGatewayConfiguration(
            ip = ip,
            secUser = user,
            secCode = key
        )
    } else
        null
}

/**
 * Create a default task with 30 minutes and `warm-white` as color. RGB lamps are currently not supported.
 */
private fun createSunriseTask(
    timestamp: Instant,
    bulbs: List<BulbSettings>,
    state: Int = 0,
    id: Int? = null
): Task = Task(
    id = id, name = "Alarm Sync Sunrise", repeat = 0, state = state, type = 4,
    startAction = Action(
        state = 1, lightSettings = bulbs.map {
            LightSetting(
                bulb = it.id,
                intensity = it.intensity,
                color = "f1e0b5", // TODO variable
                duration = 18000
            )
        }
    ),
    endAction = Action(state = 1),
    triggerIntervals = listOf(
        timestamp.let {
            val start = LocalDateTime.ofInstant(
                timestamp.minus(30, ChronoUnit.MINUTES),
                ZoneId.of("UTC+1")
            )
            val end = LocalDateTime.ofInstant(timestamp, ZoneId.of("UTC+1"))

            TriggerInterval(
                startHour = start.get(ChronoField.HOUR_OF_DAY),
                startMinute = start.get(ChronoField.MINUTE_OF_HOUR),
                endHour = end.get(ChronoField.HOUR_OF_DAY),
                endMinute = end.get(ChronoField.MINUTE_OF_HOUR)
            )
        }
    )
)

/**
 * Synchronize all saved settings with the gateway.
 * If no lamps are selected, the task gets deleted.
 */
fun Context.syncGateway() {
    val alarmManager = getSystemService(Context.ALARM_SERVICE) as? AlarmManager
    val taskId = preferences.getInt(PreferenceKeys.TaskId, 0)

    val tradfriGateway = TradfriGateway(preferences.createGatewayConfig() ?: return)
    val taskExists = tradfriGateway.safe { task(taskId) }.on({ it != null }, { false })

    val lamps = preferences.loadLampSettings()

    if (lamps.any { it.toggle } && alarmManager?.nextAlarmClock != null) {
        val timestamp = Instant.ofEpochMilli(alarmManager.nextAlarmClock.triggerTime)

        createSunriseTask(
            timestamp,
            lamps.filter { it.toggle },
            state = 1,
            id = taskId
        ).let {
            Log.d(TAG, it.toString())

            tradfriGateway.safeIf { if (taskExists) update(it) else add(it) }
                .onLog(TAG, "updated/ added task", "failed to update/ add task")
                .takeIf { !taskExists }
                ?.on {
                    preferences.edit(commit = true) {
                        putInt(
                            PreferenceKeys.TaskId,
                            tradfriGateway.tasks().lastOrNull()?.id ?: 0
                        )
                    }
                }
        }
    } else if (taskExists) {
        tradfriGateway.safeIf { deleteTask(taskId) }
            .onLog(TAG, "deleted task", "failed to delete task")
    }
}
